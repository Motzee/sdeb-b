#SDEB-LVRBEDR, notre application de voisinage


## Objectifs utilitaires
    ### Jouer
    - générateur de mots ou images pour certains jeux (basé sur une table en DB)
    - interface pour suggérer en DB de nouveaux mots
    - système de chronos persos pour pouvoir s'auto-évaluer sur certains jeux
    - listing des jeux avec règles et liens vers des vidéos en ligne

    ### Organiser des activités
    ### Partager des objets et savoirs
    Listing des objets prêtables et qui le a en garde pour l'instant

    ### Communiquer
    - possibilité de créer des forums (publics et privés)
    - salons de tchat 
    => idéalment compatibles smartphones côté notifications
    

Dématérialiser certains outils de jeux, notamment ceux autofabriqués
Partager nos photos

## Objectifs pédagogiques pour moi
Système de sessions (Breeze)
plusieurs users SQL avec des permissions différentes
Upload d'images (redimentionner si énormes et renommer)
TailwindCSS (car Breeze) et Blade

## Suivi de projet :
[ ] Coquille vide
[ ] Utilisateur SQL admin dédié + DB et paramétrage dans le projet
[ ] Listing des besoins et dégager les axes pour la version alpha
[ ] Maquettage (mockup wireframe) et diagrammes

## Todo
    ### Authentification
    [ ] Pouvoir fermer les inscriptions ou les rouvrir sur invitation
    [ ] Vérifier la récupération de mot de passe

# Mémo
php artisan serve

npm run dev

clone
composer update
php artisan generate:key
créer une DB et son user sql
copier et compléter .env
php artisan migrate
créer un user
php artisan db:seed