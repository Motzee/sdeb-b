const { isEmpty } = require('lodash');

require('./bootstrap');

require('alpinejs');

let url = "/api/games" ;

//vidage de la liste des jeux
let emptyListOfGames = function () {
    let listOfGames = document.getElementById("listOfGames") ;
    listOfGames.textContent = "" ;
}

//création d'un span de label
let createSpanForLabel = function (game, label, textForLabel, div) {
    let span = document.createElement("span");
    span.classList = "ml-3 text-sm text-gray-500 underline" ;
    span.textContent = textForLabel ;
    div.appendChild(span) ;
    div.innerHTML += " " + game[label] ;
}

//création d'un article concernant un jeu
let createArticleForGame = function (game, listOfGames) {

    var article = document.createElement("article");
    article.classList = "flex p-6" ;

    var figure = document.createElement("figure");
    figure.classList = "flex-none w-44 relative" ;

    var img = document.createElement("img");
    img.alt = "" ;
    img.classList = "inset-0 w-full h-full object-cover rounded-lg" ;
    img.src = isEmpty(game.illustration) ? "" : "/img/games_covers/" + game.illustration ;
    figure.appendChild(img);
    article.appendChild(figure);

    var div = document.createElement("div");
    div.classList = "flex-auto p-6" ;


    var h3 = document.createElement("h3");
    h3.classList = "w-full flex-none font-semibold mb-2.5" ;
    h3.innerHTML = game.title ;
    div.appendChild(h3);

    div.innerHTML += game.format ;
    div.innerHTML += " (" + game.type + ")" ;

    createSpanForLabel(game, "nbplayers_min", "Nb joueurs min :", div) ;
    createSpanForLabel(game, "nbplayers_max", "Nb joueurs max :", div) ;
    createSpanForLabel(game, "minimalage", "Âge min :", div) ;
    createSpanForLabel(game, "durationgame", "Durée :", div) ;
    createSpanForLabel(game, "", "Présentation :", div) ;

    let saut = document.createElement("br");
    div.appendChild(saut) ;

    let lnk = document.createElement("a");
    lnk.textContent = "Voir les détails" ;
    div.appendChild(lnk) ;

    article.appendChild(div);

    listOfGames.appendChild(article);
}

//remplissage de la liste des jeux
let fillListOfGames = function (games) {

    let listOfGames = document.getElementById("listOfGames") ;
    games.forEach(game => {
        createArticleForGame(game, listOfGames) ;
    });
}
/*
let btn_searchGames = document.getElementById("btn_searchGames") ;
if(btn_searchGames !== null) {
btn_searchGames.addEventListener('click', (event) => {
    event.preventDefault() ;
    var form = new FormData(document.getElementById('searchGames'));
    console.log(url);
    fetch(url, {
        method: "POST",
        body: form
    }).then(function(response) {
        var contentType = response.headers.get("content-type");
        if(contentType && contentType.indexOf("application/json") !== -1) {
          return response.json().then(function(json) {
            let games = json.games ;
            console.log(games) ;
            emptyListOfGames() ;
            fillListOfGames(games) ;
    
          });
        } else {
          console.error("Oops, la requête fetch n'a pas récupéré du JSON");
        }  
    });
}) ;
}
*/

/* Textareas avec éditeur de contenu */

tinymce.init({
    selector: '.contentEditor'
  });



/* VOCABULANGUE */
let btnShowResponse = document.getElementById('showResponse') ;
if(!!btnShowResponse) {
    let wordTranslation = document.getElementById('wordTranslation') ;

    btnShowResponse.addEventListener('click', function() {
        wordTranslation.classList.toggle("invisible");
    })
}
