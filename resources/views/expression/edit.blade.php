<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Modifier une expression') }}
        </h1>
        <a href="{{ route('expressions.edit', $expression->id + 1) }}" class="button mt-3" />Suivant</a>

            <form class="form_container" action="{{ route('expressions.update', ['expression' => $expression->id ])}}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="form_row">
                    <div class="form_col-25"><label for="label_fr" required>Label FR</label></div>
                    <div class="form_col-75"><input id="label_fr" name="label_fr" type="text" value="{{ $expression->label_fr }}" class="form-input @error('label_fr') is-invalid @enderror"  maxlength="255" required /></div>
                    @error('label_fr')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="grammarytype" required>Type grammatical</label></div>
                    <div class="form_col-75"><input id="grammarytype" name="grammarytype" type="text" value="{{ $expression->grammarytype }}" class="form-input @error('grammarytype') is-invalid @enderror"  maxlength="255" required /></div>
                    @error('grammarytype')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="pegi_vocable">PEGI de vocable</label></div>
                    <div class="form_col-75"><input id="pegi_vocable" name="pegi_vocable" value="{{ $expression->pegi_vocable }}" type="number" class="form-input" /></div>
                    @error('pegi_vocable')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="pegi_warn">PEGI warn</label></div>
                    <div class="form_col-75"><input id="pegi_warn" name="pegi_warn" value="{{ $expression->pegi_warn }}" type="number" class="form-input" /></div>
                    @error('pegi_warn')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"></div>
                    <div class="form_col-75"><input type="checkbox" name="is_guessable" id="is_guessable" value="1" {{ $expression->is_guessable ? "checked" : "" }} /> <label for="is_guessable">Est devinable</label></div>
                    @error('is_guessable')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"></div>
                    <div class="form_col-75"><input type="checkbox" name="is_drawable" id="is_drawable" value="1" {{ $expression->is_drawable ? "checked" : "" }} /> <label for="is_drawable">Est dessinable</label></div>
                    @error('is_drawable')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"></div>
                    <div class="form_col-75"><input type="checkbox" name="is_mimic" id="is_mimic" value="1" {{ $expression->is_mimic ? "checked" : "" }} /> <label for="is_mimic">Est mimable</label></div>
                    @error('is_mimic')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"></div>
                    <div class="form_col-75"><input type="checkbox" name="is_game_lynx" id="is_game_lynx" value="1" {{ $expression->is_game_lynx ? "checked" : "" }} /> <label for="is_game_lynx">Est issu du jeu du Lynx</label></div>
                    @error('is_game_lynx')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="file">Illustration</label></div>
                    <div class="form_col-75">
                        <img src="/img/{{ $expression->path_picture }}" alt="" class="vignette inset-0 w-full h-full object-cover rounded-lg" />
                        <input type="file" value="{{ $expression->path_picture }}" id="file" name="file" accept="image/png, image/jpeg"></div>
                    @error('file')
                        {{ $message }}
                    @enderror
                </div>



                <div class="form_row">
                    <div class="form_col-25"></div>
                    <div class="form_col-75"><input type="checkbox" name="is_available" id="is_available" value="{{ $expression->is_available }}" checked /> <label for="is_available">Disponible</label></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <input type="submit" value="Modifier" />
                </div>
            </form>
            
    </x-slot>
</x-app-layout>
