<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Liste des mots') }}
        </h1>

        @if (Auth::user() !== null)
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('words.create') }}"> Ajouter un mot</a>
            </div>
        @endif
        
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    <table class="table table-bordered table-responsive">
                        <tr>
                            @if (Auth::user() !== null)
                                <th>ID</th>
                            @endif
                            <th>Image</th>
                            <th>Français</th>
                            <th>Anglais</th>
                            @if (Auth::user() !== null)
                                <th>Allemand</th>
                                <th>Action</th>
                            @endif
                        </tr>
                        @foreach ($data as $key => $value)
                        <tr>
                            @if (Auth::user() !== null)
                                <td>{{ $value->id }}</td>
                            @endif
                            <td><img class="minivignette" src="{{ !!$value->picture ? $value->picture->path : '' }}" alt="pas d'image" /></td>
                            <td>{{ $value->determinant_fr }} {{ $value->label_fr }}</td>
                            <td>{{ $value->determinant_an }} {{ $value->label_an }}</td>
                            @if (Auth::user() !== null)
                                <td>{{ $value->determinant_de }} {{ $value->label_de }}</td>
                                <td>
                                    <form action="{{ route('words.destroy',$value->id) }}" method="POST">   
                                        <!--<a class="button standard" href="{ route('words.show',$value->id) }}">Voir</a>    -->
                                        <a class="button standard" href="{{ route('words.edit',$value->id) }}">Modifier</a>   
                                        @csrf
                                        @method('DELETE')      
                                        <button type="submit" class="delete">Supprimer</button>
                                    </form>
                                </td>
                            @endif
                            
                        </tr>
                        @endforeach
                    </table>  
                    {!! $data->links() !!}


                   
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
