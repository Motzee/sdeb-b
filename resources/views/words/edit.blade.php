<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Modifier un mot') }}
        </h1>

            <form class="form_container" action="{{ route('words.update', ['word' => $word->id ])}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')


                <section>
                    <h2>Français</h2>

                    <div class="form_row">
                        <div class="form_col-25"><label for="label_fr" required>Mot</label></div>
                        <div class="form_col-75"><input id="label_fr" name="label_fr" value="{{ $word->label_fr }}" type="text" class="form-input @error('label_fr') is-invalid @enderror" required /></div>
                        @error('label_fr')
                            {{ $message }}
                        @enderror
                    </div>

                    <div class="form_row">
                        <div class="form_col-25"><label for="determinant_fr">Déterminant</label></div>
                        <div class="form_col-75"><input id="determinant_fr" name="determinant_fr" value="{{ $word->determinant_fr }}" type="text" class="form-input @error('determinant_fr') is-invalid @enderror" /></div>
                        @error('determinant_fr')
                            {{ $message }}
                        @enderror
                    </div>

                    <div class="form_row">
                        <div class="form_col-25"><label for="id_typeOfWord" required>Type de mot</label></div>
                        <div class="form_col-75"><select name="id_typeOfWord" id="id_typeOfWord" required>
                            <option></option>
                            @foreach ($typeOfWordsList as $item)
                                <option value="{{ $item->id }}" {{ !empty($word['id_typeOfWord']) && $word['id_typeOfWord'] === $item->id ? "selected" : "" }}>{{ $item->type_FR }}</option>
                            @endforeach
                        </select></div>
                        @error('id_typeOfWord')
                            {{ $message }}
                        @enderror
                    </div>
                    <!--p>'verb_group'<p>
                    <p>Ajouter le type de mot avec un menu déroulant</p>
                    <p>Ajouter un système de tags</p>-->
                    <div class="form_row">
                        <div class="form_col-25"><label for="file">Image</label></div>
                        <div class="form_col-75">
                            <img class="minivignette" src="{{ !!$word->path_picture ? $word->path_picture->path : '' }}" alt="pas d'image">
                            <input type="file" value="{{ $word->file }}" id="file" name="file" accept="image/png, image/jpeg"></div>
                        @error('file')
                            {{ $message }}
                        @enderror
                    </div>

                </section>


                <section>
                        <h2>Anglais</h2>
                        
                        <div class="form_row">
                            <div class="form_col-25"><label for="label_an">Mot</label></div>
                            <div class="form_col-75"><input id="label_an" name="label_an" value="{{ $word->label_an }}" type="text" class="form-input @error('label_an') is-invalid @enderror" /></div>
                            @error('label_an')
                                {{ $message }}
                            @enderror
                        </div>

                        <div class="form_row">
                            <div class="form_col-25"><label for="determinant_an">Déterminant</label></div>
                            <div class="form_col-75"><input id="determinant_an" name="determinant_an" value="{{ $word->determinant_an }}" type="text" class="form-input @error('determinant_an') is-invalid @enderror" /></div>
                            @error('determinant_an')
                                {{ $message }}
                            @enderror
                        </div>
                    </section>

                    <section>
                        <h2>Allemand</h2>

                        <div class="form_row">
                            <div class="form_col-25"><label for="label_de">Mot</label></div>
                            <div class="form_col-75"><input id="label_de" name="label_de" value="{{ $word->label_de }}" type="text" class="form-input @error('label_de') is-invalid @enderror" /></div>
                            @error('label_de')
                                {{ $message }}
                            @enderror
                        </div>

                        <div class="form_row">
                            <div class="form_col-25"><label for="determinant_de">Déterminant</label></div>
                            <div class="form_col-75"><input id="determinant_de" name="determinant_de" value="{{ $word->determinant_de }}" type="text" class="form-input @error('determinant_de') is-invalid @enderror"  /></div>
                            @error('determinant_de')
                                {{ $message }}
                            @enderror
                        </div>
                    </section>

                <div class="form_row">
                    <input type="submit" value="Modifier" />
                </div>
            </form>

    </x-slot>
</x-app-layout>
