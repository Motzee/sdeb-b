<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Ajouter un mot') }}
        </h1>

        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('words.index') }}"> Retour à la liste</a>
        </div>
    </x-slot>



    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                <form action="{{ route('words.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <section>
                        <h2>Français</h2>

                        <div class="form_row">
                            <div class="form_col-25"><label for="label_fr" required>Mot</label></div>
                            <div class="form_col-75"><input id="label_fr" name="label_fr" type="text" class="form-input @error('label_fr') is-invalid @enderror" required /></div>
                            @error('label_fr')
                                {{ $message }}
                            @enderror
                        </div>

                        <div class="form_row">
                            <div class="form_col-25"><label for="determinant_fr">Déterminant</label></div>
                            <div class="form_col-75"><input id="determinant_fr" name="determinant_fr" type="text" class="form-input @error('determinant_fr') is-invalid @enderror"/></div>
                            @error('determinant_fr')
                                {{ $message }}
                            @enderror
                        </div>

                        <!--p>'verb_group'<p>-->
                        <div class="form_row">
                            <div class="form_col-25"><label for="id_typeOfWord" required>Type de mot</label></div>
                            <div class="form_col-75"><select name="id_typeOfWord" id="id_typeOfWord" required>
                                <option></option>
                                @foreach ($typeOfWordsList as $item)
                                    <option value="{{ $item->id }}">{{ $item->type_FR }}</option>
                                @endforeach
                            </select></div>
                            @error('id_typeOfWord')
                                {{ $message }}
                            @enderror
                        </div>
                        <!--<p>Ajouter un système de tags</p>-->

                        <div class="form_row">
                            <div class="form_col-25"><label for="file">Image</label></div>
                            <div class="form_col-75"><input type="file" id="file" name="file" accept="image/png, image/jpeg"></div>
                            @error('file')
                                {{ $message }}
                            @enderror
                        </div>
                    </section>

                    <section>
                        <h2>Anglais</h2>
                        
                        <div class="form_row">
                            <div class="form_col-25"><label for="label_an">Mot</label></div>
                            <div class="form_col-75"><input id="label_an" name="label_an" type="text" class="form-input @error('label_an') is-invalid @enderror"/></div>
                            @error('label_an')
                                {{ $message }}
                            @enderror
                        </div>

                        <div class="form_row">
                            <div class="form_col-25"><label for="determinant_an">Déterminant</label></div>
                            <div class="form_col-75"><input id="determinant_an" name="determinant_an" type="text" class="form-input @error('determinant_an') is-invalid @enderror" /></div>
                            @error('determinant_an')
                                {{ $message }}
                            @enderror
                        </div>
                    </section>

                    <section>
                        <h2>Allemand</h2>

                        <div class="form_row">
                            <div class="form_col-25"><label for="label_de">Mot</label></div>
                            <div class="form_col-75"><input id="label_de" name="label_de" type="text" class="form-input @error('label_de') is-invalid @enderror" /></div>
                            @error('label_de')
                                {{ $message }}
                            @enderror
                        </div>

                        <div class="form_row">
                            <div class="form_col-25"><label for="determinant_de">Déterminant</label></div>
                            <div class="form_col-75"><input id="determinant_de" name="determinant_de" type="text" class="form-input @error('determinant_de') is-invalid @enderror" /></div>
                            @error('determinant_de')
                                {{ $message }}
                            @enderror
                        </div>
                    </section>
                    
                      
                    <!--div class="form_row">
                        <div class="form_col-25"><label for="id_picture">Image</label></div>
                        <div class="form_col-75"><input id="id_picture" name="id_picture" type="number" class="form-input @error('id_picture') is-invalid @enderror" /></div>
                    </!--div>-->

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                    
                   
                </form>
                   
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
