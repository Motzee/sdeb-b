<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Modifier un jeu') }}
        </h1>

            <form class="form_container" action="{{ route('games.update', ['game' => $game->id ])}}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form_row">
                    <div class="form_col-25"><label for="title" required>Titre</label></div>
                    <div class="form_col-75"><input id="title" name="title" type="text" value="{{ $game->title }}" class="form-input @error('title') is-invalid @enderror"  maxlength="255" required /></div>
                    @error('title')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="shortdescription">Principe</label></div>
                    <div class="form_col-75"><textarea id="shortdescription" name="shortdescription" maxlength="255">{{ $game->shortdescription }}</textarea></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="nbPlayers" required>Nb de joueurs min</label></div>
                    <div class="form_col-75"><input id="nbplayers_min" name="nbplayers_min" value="{{ $game->nbplayers_min }}" type="number" class="form-input" required /></div>
                    @error('nbplayers_min')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="nbPlayers" required>Nb de joueurs max</label></div>
                    <div class="form_col-75"><input id="nbplayers_max" name="nbplayers_max" value="{{ $game->nbplayers_max }}" type="number" class="form-input" required /></div>
                    @error('nbplayers_max')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="minimalage">Âge minimum</label></div>
                    <div class="form_col-75"><input id="minimalage" name="minimalage" value="{{ $game->minimalage }}" type="number" class="form-input" /></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="format">Format de jeux</label></div>
                    <div class="form_col-75"><select name="format" id="format">
                        <option></option>
                        @foreach ($gameFormatsList as $item)
                            <option {{ $item->label === $game->format ? 'selected' : '' }}>{{ $item->label }}</option>
                        @endforeach
                    </select></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="affrontementtype">Système d'affrontement</label></div>
                    <div class="form_col-75"><select name="affrontementtype" id="affrontementtype">
                        <option></option>
                        @foreach ($gameAffrontementsList as $item)
                            <option {{ $item->label === $game->affrontementtype ? 'selected' : '' }}>{{ $item->label }}</option>
                        @endforeach
                    </select></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="type">Type</label></div>
                    <div class="form_col-75"><select name="type" id="type">
                        <option></option>
                        @foreach ($gameTypesList as $item)
                            <option {{ $item->label === $game->type ? 'selected' : '' }}>{{ $item->label }}</option>
                        @endforeach
                    </select></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="capacityneeded">Capacités mises en jeu</label></div>
                    <div class="form_col-75">
                        @foreach ($gameCapacitesList as $item)
                            <input type="checkbox" name="capacityneeded[]" id="{{ $item->label }}" value="{{ $item->label }}" {{ $item->label === $game->type ? 'checked' : '' }}/> <label for="{{ $item->label }}">{{ $item->label }}</label>
                        @endforeach
                    </div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                
                <div class="form_row">
                    <div class="form_col-25"><label for="durationgame">Durée</label></div>
                    <div class="form_col-75"><input id="durationgame" value="{{ $game->durationgame }}" name="durationgame" type="text" value="00:30:00"/></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"></div>
                    <div class="form_col-75"><input type="checkbox" name="is_gamemasterneeded" value="{{ $game->is_gamemasterneeded }}" id="is_gamemasterneeded" /> <label for="is_gamemasterneeded">Maître du jeu nécessaire</label></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="rules_text">Règles</label></div>
                    <div class="form_col-75"><textarea id="rules_text" name="rules_text" class="contentEditor">{{ $game->rules_text }}</textarea></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="rules_video">Règles en vidéo (lien) :</label></div>
                    <div class="form_col-75"><input id="rules_video" value="{{ $game->rules_video }}" name="rules_video" type="text"/></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="file">Illustration</label></div>
                    <div class="form_col-75"><input type="file" value="{{ $game->file }}" id="file" name="file" accept="image/png, image/jpeg"></div>
                    @error('file')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="content">Contenu de base</label></div>
                    <div class="form_col-75"><textarea id="content" name="content" class="contentEditor">{{ $game->content }}</textarea></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"></div>
                    <div class="form_col-75"><input type="checkbox" name="is_available" id="is_available" value="{{ $game->is_available }}" checked /> <label for="is_available">Disponible</label></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>

                <div class="form_row">
                    <input type="submit" value="Modifier" />
                </div>
            </form>

    </x-slot>
</x-app-layout>
