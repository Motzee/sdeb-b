<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between items-center flex-wrap">
            <h1>
                {{ __('Ludothèque partagée') }}
            </h1>

            @if (Auth::user() !== null)
                <a href="/games/create" class="button" title="Ajouter un jeu que vous possédez à votre liste personnelle">Ajouter un jeu</a>
            @endif
        </div>

        <section><h2>Recherche de jeux par critères</h2>
            <form id="searchGames"  action="{{ route('games.index') }}" method="GET" >

                <div class="flex justify-between items-center flex-wrap">
                    <div class="">
                        <label for="game_title">Titre</label>
                        <input id="game_title" name="game_title" type="text" class="form-input" value="{{ !empty($searchParameters['game_title']) ? $searchParameters['game_title']: "" }}"/>
                    </div>

                    <div class="">
                        <label for="game_nbPlayers" class="ml-2">Nb de joueurs</label>
                        <input id="game_nbPlayers" name="game_nbPlayers" type="number" class="form-input"  value="{{ !empty($searchParameters['game_nbPlayers']) ? $searchParameters['game_nbPlayers'] : "" }}"/>
                    </div>

                    <div class="ml-2">
                        @if (Auth::user() !== null)
                            <input type="checkbox" name="onlyMyGames" id="onlyMyGames" {{ !empty($searchParameters['onlyMyGames']) ? "checked" : "" }}/> <label for="onlyMyGames">N'afficher que vos jeux</label>
                        @endif
                    </div>
                    
                </div>

                <div class="form_row">
                    <div class="form_col-25"><label for="format">Format de jeux</label></div>
                    <div class="form_col-75"><select name="format" id="format">
                        <option></option>
                        @foreach ($gameFormatsList as $item)
                            <option {{ !empty($searchParameters['format']) && $searchParameters['format'] === $item->label ? "selected" : "" }}>{{ $item->label }}</option>
                        @endforeach
                    </select></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="affrontementtype">Système d'affrontement</label></div>
                    <div class="form_col-75"><select name="affrontementtype" id="affrontementtype">
                        <option></option>
                        @foreach ($gameAffrontementsList as $item)
                            <option {{ !empty($searchParameters['affrontementtype']) && $searchParameters['affrontementtype'] === $item->label ? "selected" : "" }}>{{ $item->label }}</option>
                        @endforeach
                    </select></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>
                
                <div class="form_row">
                    <div class="form_col-25"><label for="type">Type</label></div>
                    <div class="form_col-75"><select name="type" id="type">
                        <option></option>
                        @foreach ($gameTypesList as $item)
                            <option {{ !empty($searchParameters['type']) && $searchParameters['type'] === $item->label ? "selected" : "" }}>{{ $item->label }}</option>
                        @endforeach
                    </select></div>
                    @error('')
                        {{ $message }}
                    @enderror
                </div>

                <!--div class="form_row">
                    <div class="form_col-25"><label for="capacityneeded">Capacités mises en jeu</label></div>
                    <div class="form_col-75 flex items-center flex-wrap">
                        foreach ($gameCapacitesList as $item)
                            <div class="whitespace-nowrap mr-6">
                                <input type="checkbox" name="capacityneeded[]" id="{ $item->label }}" value="{ $item->label }}" /> <label for="{ $item->label }}">{ $item->label }}</label>
                            </div>
                        endforeach
                    </div>
                    error('')
                        { $message }}
                    enderror
                </div-->

                
            
                <input class="" type="submit" id="btn_searchGames" value="Rechercher" />
            </form>

        </section>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    

                    <section><h2>Liste de jeux</h2>
                        <div id="listOfGames">
                        @empty($games)
                            Il n'y a aucun jeu actuellement disponible en base de données
                        @endempty
                            @foreach ($games as $game)
                            <article class="flex flex-wrap p-6">
                                <figure class="flex-none w-44 relative">
                                    @if (empty($game->illustration) )
                                        <img src="/img/games_covers/test.png" alt="" class="inset-0 w-full h-full object-cover rounded-lg" />
                                    @else
                                        <img src="/img/games_covers/{{ $game->illustration }}" alt="" class="inset-0 w-full h-full object-cover rounded-lg" />
                                    @endif
                                </figure>

                                @if (empty($game->user_id_borrower) AND empty($game->externalborrower_name))
                                    <div class="flex-auto p-6 ">
                                @else
                                    <div class="flex-auto p-6 opacity-25">
                                    <span class="ml-3 text-sm text-gray-500 underline">Emprunté par :</span> {{ $game->externalborrower_name }} {{ $game->user_id_borrower }}
                                @endif
                                    <h3 class="w-full flex-none font-semibold mb-2.5">{{ $game->title }}</h3>
                                    <span class="ml-3">{{ $game->format }} @if (!empty($game->type))
                                        ({{ $game->type }})
                                    @endif</span><br/>
                                    <span class="ml-3 text-sm text-gray-500 underline">Nb joueurs :</span> {{ $game->nbplayers_min }} à {{ $game->nbplayers_max }} (à partir de {{ $game->minimalage }} ans)<br/>
                                    <span class="ml-3 text-sm text-gray-500 underline">Durée :</span> {{ $game->durationgame }}<br/>
                                    <span class="ml-3 text-sm text-gray-500 underline">Présentation :</span> {{ $game->shortdescription }}
                                    @if ($game->is_gamemasterneeded === 1)
                                        Nécessite un·e Maître Du Jeu
                                    @endif
                                    <br/><a href="{{ route('games.show', $game->slug) }}" title="Consulter la page dédiée à ce jeu" class="button mt-3">Voir les détails</a>
                                </div>
                            </article>
                            @endforeach
                        </div>
                    </section>
                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
