<x-app-layout>
    <x-slot name="header">
        <div class="flex flex-wrap items-center">
            <h1 class="mr-2">
                {{ __('Détails de '.$game->title) }}
            </h1>
            @if (Auth::user() !== null)
                <!-- Suppression -->
                <form method="POST" action="{{ route('games.destroy', $game) }}">
                    @csrf
                    @method('DELETE')
                    <button class="delete" type="submit">Supprimer</button>
                </form>
                <a href="{{ route('games.edit', $game) }}" class="button" />Modifier</a>
            @endif
        </div>
        <div class="">
            <div class="">
                <img src="/img/games_covers/{{ $game->illustration }}" alt="" class="illustration inset-0 w-full h-full object-cover rounded-lg" />
                @if (Auth::user() !== null)
                    <div class="">
                        <p>Possédé par :</p>
                        @foreach ($owners as $anOwner)
                            <img src="/img/users_avatars/{{ $anOwner->avatar }}" alt="" title="{{ $anOwner->name }} {{ false ? "(emprunté par quelqu'un·e)" : "" }}" class="minivignette inset-0 w-full h-full object-cover rounded-lg {{ $game->is_available !== 1 ? "unavailable" : "" }}" />
                        @endforeach
                    </div>
                @endif
            </div>
            <div>
                {{ $game->format }} ({{ $game->type }}) {{ $game->affrontementtype }} {{ $game->capacityneeded }}
                <p><span class="ml-3 text-sm text-gray-500 underline">Nb de joueurs :</span> {{ $game->nbplayers_min }} -> {{ $game->nbplayers_max }} ({{ $game->minimalage }} ans minimum)</p>
                <p><span class="ml-3 text-sm text-gray-500 underline">Durée :</span> {{ $game->durationgame }}</p>
                <p><span class="ml-3 text-sm text-gray-500 underline">Présentation :</span> {{ $game->shortdescription }}</p>
                @if ($game->is_gamemasterneeded === 1)
                <p>Nécessite un·e Maître Du Jeu</p>
                @endif
                <span class="ml-3 text-sm text-gray-500 underline">Règle :</span> {{ $game->rules_text }}
                <p><span class="ml-3 text-sm text-gray-500 underline"><a href="{{ $game->rules_video }}">Règle (vidéo)</a></span> </p>
                <span class="ml-3 text-sm text-gray-500 underline">Contenu du jeu :</span> {{ $game->content }}
            </div>

            <a href="{{ route('games.index') }}" title="Revenir à la liste" class="button" />Retour à la liste</a>
        </div>
    </x-slot>

</x-app-layout>
