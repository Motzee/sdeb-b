<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Administrer l\'application') }}
        </h1>
    </x-slot>

    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">

        <section><h2>Liste des utilisateurices</h2>
            <div class="row mb-4">
                <p>Les inscriptions sont actuellement ouvertes/fermées</p>
                <form>
                    @csrf            
                    <button id="btn_searchGames">Ouvrir/Fermer</button>
                </form>
            </div>
            

            <table class="table">
                <thead class="thead">
                  <tr>
                    <th scope="col">Role</th>
                    <th scope="col">Identité</th>
                    <th scope="col">Inscription<br/>Dernière connexion</th>
                    <th scope="col">Statut</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody class="tbody">
                    @foreach ($users as $anUser)
                        <tr  class="{{ $anUser->is_available === 0 ? 'table-secondary' : ''}} {{ $anUser->deleted_at !== null ? 'table-danger' : ''}}">
                            <td>{{ $anUser->role }}</td>
                            <td>{{ $anUser->name }} (id {{ $anUser->id }})<br/>
                                {{ $anUser->email }}
                            </td>
                            <td>{{ date('d/m/Y', strtotime($anUser->created_at)) }}<br/>
                                {{ $anUser->dateFormatFR(($anUser->updated_at)) }}
                            </td>
                            <td>
                                @if ($anUser->deleted_at !== null)
                                    Désinscrit·e
                                @elseif($anUser->is_available === 0)
                                    Désactivé·e
                                @else
                                    Inscrit·e
                                @endif
                            </td>
                            <td><button>…</button></td>
                        </tr>
                    @endforeach
                  

                </tbody>
              </table>

        </section>

        <!-- --------------------------------------------------- -->

        <section><h2>Gestion des expressions</h2>
            <a href="{{ route('admin.expressions') }}" class="button mt-3" />Voir la liste</a>
        </section>
    </div>
</x-app-layout>
