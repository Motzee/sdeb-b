<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Votre profil') }}
        </h1>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <section><h2>Vos informations</h2>
                        <div class="flex flex-wrap">
                            <img src="/img/users_avatars/{{ $user->avatar }}" alt="" class="vignette inset-0 w-full h-full object-cover rounded-lg mr-2 mb-2" />
                            <form class="form_container" action="{{ route('updateUserProfile')}}" method="POST"  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form_row">
                                    <div class="form_col-25"><label for="name" required>Pseudo</label></div>
                                    <div class="form_col-75"><input id="name" name="name" type="text" value="{{ $user->name }}" class="form-input @error('name') is-invalid @enderror"  maxlength="255" required /></div>
                                    @error('name')
                                        {{ $message }}
                                    @enderror
                                </div>

                                <div class="form_row">
                                    <div class="form_col-25"><label for="email" required>E-mail</label></div>
                                    <div class="form_col-75"><input id="email" name="email" type="text" value="{{ $user->email }}" class="form-input @error('email') is-invalid @enderror"  maxlength="255" required /></div>
                                    @error('email')
                                        {{ $message }}
                                    @enderror
                                </div>

                                <div class="form_row">
                                    <div class="form_col-25"><label for="file">Avatar</label></div>
                                    <div class="form_col-75"><input type="file" value="{{ $user->avatar }}" id="file" name="file" accept="image/png, image/jpeg"></div>
                                    @error('file')
                                        {{ $message }}
                                    @enderror
                                </div>

                                <div class="form_row">
                                    <input type="submit" value="Modifier" />
                                </div>

                            </form>
                        </div>
                    </section>

                    <section style="clear:both;"><h2>Changer de mot de passe</h2>
                        <p>Pour changer de mot de passe, ça va venir…</p>
                    </section>

                    <section><h2>Vous désinscrire</h2>
                        <p>Pour supprimer votre compte sur cette présente plateforme, complétez le mini-formulaire ci-dessous. Cela aura pour effet de supprimer irrémédiablement :</p>
                        <ul>
                            <li>votre compte personnel</li>
                            <li>vos exemplaires de jeux</li>
                        </ul>
                        <p>Cela ne supprimera pas les jeux d'autres personnes, ni les expressions que vous avez renseignées sur la plateforme.</p>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
