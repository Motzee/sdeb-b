<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Administrer l\'application') }}
        </h1>
    </x-slot>

    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">

        <!-- --------------------------------------------------- -->

        <section><h2>Expressions pour le lynx</h2>
            <div class="flex flex-wrap mb-4">
                @foreach ($expressionsLynx as $anExp)
                    <div style="width:125px">
                        <img src="/img/{{ $anExp->path_picture }}" alt="{{ $anExp->label_fr }}" class="vignette" />
                        {{ $anExp->label_fr }}&nbsp;<a href="{{ route('expressions.edit', $anExp->id) }}" title="Consulter la page d'édition de cette expression'" class="" />📝</a>
                    </div>
                @endforeach
            </div><br/>
            <div>
                {{ $expressionsLynx->links() }}
            </div>

        </section>

        <!-- --------------------------------------------------- -->

        <section><h2>Expressions autres que pour le lynx</h2>
            <div class="row mb-4">
                @foreach ($expressionsAutres as $anExp)
                    {{ $anExp->label_fr }} <a href="{{ route('expressions.edit', $anExp->id) }}" title="Consulter la page d'édition de cette expression'" class="" />📝</a><br/>
                @endforeach
            </div>
            


        </section>
    </div>
</x-app-layout>
