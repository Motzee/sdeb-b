<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Bienvenue dans un quartier SDeB !') }}
        </h1>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <p>Ici ça joue, mais c'est privé, il faudra donc vous connecter pour participer. Vous êtes du quartier et vous avez envie d'avoir un compte ? Allez voir Maëlle et demandez-lui de rouvrir les inscriptions :-).</p>
                </div>
            </div>
        </div>
    </div>


</x-app-layout>
