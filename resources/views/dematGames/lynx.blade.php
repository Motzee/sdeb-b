<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Jeu du Lynx Freepik') }}
        </h1>
    </x-slot>
    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <p>Disposez les tuiles à images multiples sur la table (total : 504 images), puis retrouvez les images suivantes :</p>
        <div class="flex flex-wrap">
            @foreach ($expressionsLynx as $anExpressionLynx)
                <div>
                    <img src="/img/{{ $anExpressionLynx->path_picture }}" alt="{{ $anExpressionLynx->label_fr }}" class="vignette" />{{ $anExpressionLynx->label_fr }}
                </div>
            @endforeach
        </div>
        <p>Actualisez cette page pour obtenir un autre set de 5 images</p>
    </div>
    <!--
    Tangram
    -->
</x-app-layout>
