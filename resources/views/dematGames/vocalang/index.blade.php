<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Vocabulangue, comment dit-on') }}
        </h1>
    </x-slot>
    <div class="max-w-7xl mx-auto py-4 px-4 sm:px-6 lg:px-8">
        @foreach ($words as $aWord)
            <div class="flex flex-col justify-center items-center">
                
                <p class="flex flex-wrap items-center">
                    <img class="microdrapeau mr-2" src="{{ $language->path_flag }}" alt="{{ $language->label }}" />
                    <span class="text-2xl">{{ $aWord->{"determinant_".$language->iso_639_1} }} {{ $aWord->{"label_".$language->iso_639_1} }}</span>
                </p>
                <img class="illustration" src="{{ $aWord->path }}" alt="" class="vignette" />
                <p class="flex flex-wrap items-center text-2xl">
                    <img class="microdrapeau mr-2" src="{{ $chosenLanguage->path_flag }}" alt="{{ $chosenLanguage->label }}" />
                    <span id="wordTranslation" class="invisible">{{ $aWord->{"determinant_".$chosenLanguage->iso_639_1} }} {{ $aWord->{"label_".$chosenLanguage->iso_639_1} }}</span>
                </p>
            </div>
        @endforeach

        <div class="flex flex-wrap align-center justify-center mb-4 mt-2">
            <button id="showResponse">Réponse</button>
        </div>

        {{ $words->links() }}

    </div>

</x-app-layout>