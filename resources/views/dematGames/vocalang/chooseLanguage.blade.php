<x-app-layout>
    <x-slot name="header">
        <h1>
            {{ __('Vocabulangue') }}
        </h1>
    </x-slot>
    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <!-- Quizz (mini-jeu) -->

        <section>
            <h2>Flashcards</h2>

            <p>Touche le drapeau, et devine comment on dit les mots dans cette langue :</p>
            <div class="flex flex-wrap">
                @foreach ($chosenLanguage as $aLang)
                    <a class="mr-2" href="{{ route('dematerialized-games.chosenVocalang', ["language_iso" => $language->iso_639_1, "chosenLanguage_iso" => $aLang->iso_639_1]) }}"><img src="{{ $aLang->path_flag }}" alt="drapeau" class="vignette" />{{ $aLang->label }}</a>
                @endforeach
            </div>
            <p>Cet outil de révision n'a pas de système de score : révise autant qu'il te plait !</p>
        </section>

        <section>
            <h2>Consulte <a style="border-bottom :2px dashed purple" href="{{ route('words.index') }}">la liste de mots</a></h2>
        </section>
        
        <p>NB : Les images sont issues du site <a href="https://www.flaticon.com" target="_blank" rel="noopener noreferrer">Flaticon</a></p>
    </div>

</x-app-layout>