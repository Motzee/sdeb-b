<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Picture extends Model
{
    use HasFactory, SoftDeletes;
/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pictures';

    protected $fillable = [
        'path',
        'is_available',
        'deleted_at'
    ] ;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true ;


    /**
     * Get the words for the Picture
     */
    public function words() {
        return $this->hasMany(Word::class);
    }
}
