<?php

namespace App\Models;

use App\Models\Tag;
use App\Models\Picture;
use App\Models\TypeOfWord;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Word extends Model {
    use HasFactory, SoftDeletes ;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'words';

    protected $fillable = [
        'label_fr',
        'determinant_fr',
        'verb_group',
        'label_an',
        'determinant_an',
        'label_de',
        'determinant_de',
        'label_br',
        'determinant_br',
        'label_eo',
        'determinant_eo',
        'id_typeOfWord',
        'id_picture',
        'is_available'
    ];

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true ;
    
    /**
     * Get the type that owns the word
     */
    public function typeOfWord() {
        return $this->belongsTo(TypeOfWord::class, 'id_typeOfWord');
    }

    /**
     * Get the picture that owns the word
     */
    public function picture() {
        return $this->belongsTo(Picture::class, 'id_picture');
    }

    /**
     * The tags that belong to the word.
     */
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }

    public static function boot() {
        parent::boot();

        static::deleting(function ($word) {
            $word->deleted_by = Auth::id(); 
            $word->save();
        });
    }
}
