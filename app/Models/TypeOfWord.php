<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeOfWord extends Model {
    use HasFactory;

    /**
     * Get the words for the Type.
     */
    public function words() {
        return $this->hasMany(Word::class);
    }
}
