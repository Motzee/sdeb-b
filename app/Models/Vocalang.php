<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Vocalang extends Model
{
    use HasFactory, SoftDeletes ;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vocalangs';

    protected $fillable = [
        'label',
        'iso_639_1',
        'path_flag',
        'is_available'
    ];

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true ;


    /**
     * The users that chose that lang.
     */
    public function users() {
        return $this->belongsToMany(User::class, 'users_vocalangs', 'id_vocalang', 'id_user');
    }
}
