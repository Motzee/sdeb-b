<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Game extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'games';

    protected $fillable = [
        'user_id_borrower',
        'title',
        'slug',
        'shortdescription',
        'affrontementtype',
        'capacityneeded',
        'type',
        'format',
        'durationgame',
        'is_gamemasterneeded',
        'nbplayers_min',
        'nbplayers_max',
        'minimalage',
        'content',
        'rules_text',
        'rules_video',
        'illustration',
        'externalborrower_name',
        'is_available',
        'deleted_at'
    ] ;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true ;

    /**
     * The users that get the game.
     */
    public function owners() {

        return $this->belongsToMany(User::class, 'game_user');

    }
}
