<?php

namespace App\Models;

use App\Models\Vocalang;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    const ROLE_USER = 'USER' ;
    const ROLE_ADMIN = 'ADMIN' ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'avatar',
        'is_available'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The games that belong to the user.
     */
    public function games() {
        return $this->belongsToMany(Game::class, 'game_user');
    }

    /**
     * The langs for the user.
     */
    public function vocalangs() {
        return $this->belongsToMany(Vocalang::class,  'users_vocalangs', 'id_user', 'id_vocalang');
    }



    public function dateFormatFR($date) {
        return date_format($date, 'd/m/Y') ;
    }
}
