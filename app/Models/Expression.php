<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Expression extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'label_fr',
        'is_guessable',
        'is_drawable',
        'is_mimic',
        'is_game_lynx',
        'grammarytype',
        'path_picture',
        'file',
        'pegi_vocable',
        'pegi_warn',
        'is_available',
        'deleted_at',
    ] ;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'expressions';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
