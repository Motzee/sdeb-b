<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Game ;
use App\Observers\GameObserver ;

class AppServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Game::observe(GameObserver::class) ;
    }
}
