<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExpressionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label_fr'              => 'required|max:255',
            'is_guessable'          => 'boolean',
            'is_drawable'           => 'boolean',
            'is_mimic'              => 'boolean',
            'is_game_lynx'          => 'boolean',
            'grammarytype'          => '',
            'path_picture'          => '',
            'file'                  => 'file|mimes:jpg,jpeg,png,gif,bmp',
            'pegi_vocable'          => '',
            'pegi_warn'             => '',
            'is_available'          => '',
        ];
    }
}
