<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title'                 => 'required|max:255',
            'shortdescription'      => '',
            'affrontementtype'      => '',
            'capacityneeded'        => '',
            'type'                  => '',
            'format'                => '',
            'durationgame'          => '',
            'is_gamemasterneeded'   => '',
            'nbplayers_min'         => 'required',
            'nbplayers_max'         => 'required',
            'minimalage'            => '',
            'content'               => '',
            'rules_text'            => '',
            'rules_video'           => '',
            'illustration'          => '',
            'file'                  => 'mimes:jpg,jpeg,png,gif,bmp',
            'is_available'          => ''
        ];
    }


    /**
     * Customize error messages
     *
     * @return array
     */
    public function messages() {
        return [
            'title.required'            => 'Le champ Nom est requis',
            'title.max'                 => 'Le champ Nom ne peut excéder 255 caractères',
            /*'illustration.mimes'        => 'L\'illustration doit être un fichier jpg ou png',
            'illustration.max'          => 'L\'illustration semble trop volumineuse'*/
        ];
    }
}
