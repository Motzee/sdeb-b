<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label_fr'          => 'required',
            'determinant_fr'    => '',
            'id_typeOfWord'    => 'required',
            'label_an'          => '',
            'determinant_an'    => '',
            'label_de'          => '',
            'determinant_de'    => '',
            'path_picture'    => '',
        ];
    }
}
