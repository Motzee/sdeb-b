<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'max:255',
            'email'                 => 'max:255',
            'password'              => '',
            'role'                  => 'max:255',
            'avatar'                => 'max:255',
            'file'                  => 'mimes:jpg,jpeg,png,gif,bmp',
            'is_available'          => ''
        ];
    }
}
