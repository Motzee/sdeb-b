<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Expression;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;

class ParametersController extends Controller
{
    //USER
    public function userProfile() {
        $user = Auth::user() ;

        return view('parameters.userProfile', [
            'user'             => $user
        ]);
    }

    /**
     * Update the user in storage.
     *
     * @param  App\Http\Requests\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(UserRequest  $request) {
        
        $user = Auth::user() ;

        $validatedData = $request->validated() ;
        
        if($request->hasFile('file')) {
            // Upload file
            $file = $request->file;
            $filename = Str::slug($request->name, '_') . '.' . $file->extension() ;
            $location = 'img/users_avatars/';
            $file->move($location,$filename);

            $validatedData['avatar'] = $filename ;
        }
        
        $user->update($validatedData);

        return redirect()->route('userProfile')->with('success', 'Votre profil a bien été modifié');
    }
    

    //ADMIN
    public function administration() {
        $users = User::select(['id', 'name', 'email', 'email_verified_at', 'role', 'is_available', 'created_at', 'updated_at', 'deleted_at'])->get() ;

        return view('parameters.administration', [
            'users'             => $users
        ]);
    }

    public function administrateExpressions() {
        $expressionsLynx = Expression::where([
            ['is_game_lynx', '=', 1],
        ])->paginate(50);
        $expressionsAutres = Expression::where([
            ['is_game_lynx', '=', 0],
        ])->get();

        return view('parameters.adminExpressions', [
            'expressionsLynx'   => $expressionsLynx,
            'expressionsAutres' => $expressionsAutres
        ]);
    }
}
