<?php

namespace App\Http\Controllers;

use App\Models\Word;
use App\Models\Picture;
use Illuminate\Support\Str;
use App\Http\Requests\WordRequest;
use Illuminate\Http\Request;
use App\Manager\TypeOfWordManager;
use Illuminate\Support\Facades\Auth;

class WordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = Word::latest()->paginate(10);
    
        return view('words.index',compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }

        $typeOfWordsManager = new TypeOfWordManager ;

        return view('words.create', [
            'typeOfWordsList'      => $typeOfWordsManager->getTypesList()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\WordRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WordRequest $request) {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }

        $validatedData = $request->validated() ;

        if($request->hasFile('file')) {
            // Upload file
            $file = $request->file;
            $filename = Str::slug($request->label_fr, '_') . '.' . $file->extension() ;
            $location = 'img/flashcards/';
            $file->move($location,$filename);

            $picture = Picture::create(['path' => '/'.$location.$filename]) ;

            $validatedData['id_picture'] = $picture->id ;
        } else {
            $validatedData['id_picture'] = null ;
        }

        Word::create($validatedData);
     
        return redirect()->route('words.index')
                        ->with('success','Le mot a été ajouté avec succès');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Word  $word
     * @return \Illuminate\Http\Response
     */
    /*public function show(Word $word) {
        return view('words.show',compact('word'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function edit(Word $word) {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }

        $typeOfWordsManager = new TypeOfWordManager ;

        return view('words.edit', [
            'word'                  => $word,
            'typeOfWordsList'       => $typeOfWordsManager->getTypesList()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\WordRequest  $request
     * @param  \App\Models\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function update(WordRequest $request, Word $word) {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }
    
        $validatedData = $request->validated() ;
        
        if($request->hasFile('file')) {
            // Upload file
            $file = $request->file;
            $filename = Str::slug($request->label_fr, '_') . '.' . $file->extension() ;
            $location = 'img/flashcards/';
            $file->move($location,$filename);

            $picture = Picture::create(['path' => '/'.$location.$filename]) ;

            $validatedData['id_picture'] = $picture->id ;
        }
        
        $word->update($validatedData);
    
        return redirect()->route('words.index')
                        ->with('success','Le mot a été modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function destroy(Word $word) {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }

        $word->delete();
    
        return redirect()->route('words.index')
                        ->with('success','Mot supprimé');
    }
}
