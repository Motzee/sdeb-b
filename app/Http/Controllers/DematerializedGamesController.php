<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Word;

use App\Models\Vocalang;
use App\Models\Expression;
use Illuminate\Http\Request;
use App\Http\Requests\GameRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DematerializedGamesController extends Controller {

    /**
     * Display a page
     *
     * @return \Illuminate\Http\Response
     */
    public function lynx() {

        $expressionsLynx = Expression::where([
                ['is_game_lynx', '=', 1],
                ['is_available', '=', 1],
                ['deleted_at', '=', NULL],
            ])->inRandomOrder()->limit(5)->get();
        //dd($expressionsLynx);
        return view('dematGames.lynx', [
            'expressionsLynx' => $expressionsLynx
        ]);
    }

    /**
     * Display a page showing options
     *
     * @return \Illuminate\Http\Response
     */
    public function vocalang() {

        $user = Auth::user() ;

        if($user === null) {
            $language = Vocalang::where([
                ['iso_639_1', '=', 'fr'],
                ['is_available', '=', 1],
                ['deleted_at', '=', NULL],
            ])->first();
            $chosenLanguage = Vocalang::where([
                ['iso_639_1', '=', 'an'],
                ['is_available', '=', 1],
                ['deleted_at', '=', NULL],
            ])->get();
        } else {
            $language = DB::table('users')
            ->join('users_vocalangs', 'users.id', '=', 'users_vocalangs.id_user')
            ->join('vocalangs', 'vocalangs.id', '=', 'users_vocalangs.id_vocalang')
            ->select('vocalangs.label', 'vocalangs.iso_639_1', 'vocalangs.path_flag' )
            ->where([
                ['users.id', '=', $user->id],
                ['users_vocalangs.is_mothertongue', '=', 1],
                ['vocalangs.is_available', '=', 1],
                //['vocalangs.deleted_at', '<>', null],
            ])->first() ;
            
            $chosenLanguage = DB::table('users')
            ->join('users_vocalangs', 'users.id', '=', 'users_vocalangs.id_user')
            ->join('vocalangs', 'vocalangs.id', '=', 'users_vocalangs.id_vocalang')
            ->select('vocalangs.label', 'vocalangs.iso_639_1', 'vocalangs.path_flag' )
            ->where([
                ['users.id', '=', $user->id],
                ['users_vocalangs.is_mothertongue', '<>', 1],
                ['vocalangs.is_available', '=', 1],
                //['vocalangs.deleted_at', '<>', null],
            ])->get() ;
        }
        if(is_null($language) || is_null($chosenLanguage)) {
            dd("Erreur, une des langues d'apprentissage n'est pas disponible : si vous disposez d'un compte, allez dans votre page de profil pour sélectionner vos langues") ;
        }

        //display a page to choose which language practice
        return view('dematGames.vocalang.chooseLanguage', [
            'language'          => $language,
            'chosenLanguage'    => $chosenLanguage
        ]);
    }

    /**
     * Display a page
     *
     * @return \Illuminate\Http\Response
     */
    public function chosenVocalang(string $language_iso, string $chosenLanguage_iso) {

        $language = Vocalang::where([
            ['iso_639_1', '=', strtolower($language_iso)],
            ['is_available', '=', 1],
            ['deleted_at', '=', NULL],
        ])->first() ;
        
        $chosenLanguage = Vocalang::where([
            ['iso_639_1', '=', strtolower($chosenLanguage_iso)],
            ['is_available', '=', 1],
            ['deleted_at', '=', NULL],
        ])->first() ;

        if(is_null($language) || is_null($chosenLanguage)) {
            dd("L'une des langues choisies n'est pas supportée :-/");
        }

        //Select all words which exist for the 2 selected languages (picture is facultative -> right join)
        $lang_iso = $language->iso_639_1 ;
        $chosenLang_iso = $chosenLanguage->iso_639_1 ;
        $words = DB::table('pictures')
            ->rightJoin('words', 'pictures.id', '=', 'words.id_picture')
            ->select('words.label_'.$lang_iso, 'words.determinant_'.$lang_iso, 'words.label_'.$chosenLang_iso, 'determinant_'.$chosenLang_iso, 'words.id_picture', 'pictures.path')
            ->where([
                ['words.label_'.$lang_iso, '<>', null],
                ['words.label_'.$chosenLang_iso, '<>', null],
                ['words.is_available', '=', 1]
            ])
            ->orderBy('words.id', 'desc')
            ->paginate(1);

        return view('dematGames.vocalang.index', [
            'words'             => $words,
            'language'          => $language,
            'chosenLanguage'    => $chosenLanguage
        ]);
    }
}
