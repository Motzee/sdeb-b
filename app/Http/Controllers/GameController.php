<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\User;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Characteristic;
use App\Http\Requests\GameRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Manager\CharacteristicManager;

class GameController extends Controller {

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @param  Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        /*//get user's games
        $user = Auth::user() ;
        $games = $user->games()->orderBy('title')->get() ;

        return view('game.list', [
            'games' => $games
        ]);*/

        

        $sqlWhereConditions = [['games.is_available', '=', '1']] ;

        //si on a reçu des paramètres de recherche, on ajoute les critères à la requête SQL en préparation
        if(!empty($request->all())) {
            if(!empty(request("game_title"))) {
                $sqlWhereConditions[] = ['title','LIKE','%'.request("game_title").'%'] ;
            }
    
            if(!empty(request("game_nbPlayers"))) {
                $sqlWhereConditions[] = ['nbplayers_min','<=',request("game_nbPlayers")] ;
                $sqlWhereConditions[] = ['nbplayers_max','>=',request("game_nbPlayers")] ;
            }
    
            if(!empty(request("format"))) {
                $sqlWhereConditions[] = ['format','=',request("format")] ;
            }
            if(!empty(request("affrontementtype"))) {
                $sqlWhereConditions[] = ['affrontementtype','=',request("affrontementtype")] ;
            }
            if(!empty(request("type"))) {
                $sqlWhereConditions[] = ['type','=',request("type")] ;
            }
            /*
            if(!empty(request("capacityneeded"))) {
                $sqlWhereConditions[] = ['capacityneeded','LIKE','%'.request("capacityneeded").'%'] ;
            }*/
        }

        //si l'user a demandé la liste de ses jeux, on fera une jointure
        if(!empty(request("onlyMyGames"))) {
            $userId = Auth::user()->id ;

            $games = DB::table('games')
                ->join('game_user', 'games.id', '=', 'game_user.game_id')
                ->where($sqlWhereConditions)
                ->where('game_user.user_id', '=', $userId)
                ->orderBy('title')
                ->get();
        } else {
            $games = DB::table('games')->where($sqlWhereConditions)->orderBy('title')->get();
        }

        $characteristicsManager = new CharacteristicManager ;

        return view('game.list', [
            'games'                     => $games,
            'searchParameters'          => $request->all(),
            'users_byId'                => User::select(['id', 'name'])->orderBy('id')->get()->keyBy('id'),
            'gameTypesList'             => $characteristicsManager->getTypesList(),
            'gameAffrontementsList'     => $characteristicsManager->getAffrontementsList(),
            'gameFormatsList'           => $characteristicsManager->getFormatsList(),
            'gameCapacitesList'         => $characteristicsManager->getCapacitesList(),
        ]);
    }

    /**
     * Display a form to create a newly resource in storage.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $characteristicsManager = new CharacteristicManager ;
        
        return view('game.create', [
            'gameTypesList'             => $characteristicsManager->getTypesList(),
            'gameAffrontementsList'     => $characteristicsManager->getAffrontementsList(),
            'gameFormatsList'           => $characteristicsManager->getFormatsList(),
            'gameCapacitesList'         => $characteristicsManager->getCapacitesList(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\GameRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GameRequest $request) {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }
        
        $userId = Auth::id() ;

        $validatedData = $request->validated() ;
        $validatedData['user_id_owner'] = $userId ;
        
        //isMethod('GET')
        
        //$request->all()
        //$request->input('monChamp')
        //$request->missing('monChamp')
        
        if($request->hasFile('file')) {
            // Upload file
            $file = $request->file;
            $filename = Str::slug($validatedData['title'], '_') . '.' . $file->extension() ;
            $location = 'img/games_covers/';
            $file->move($location,$filename);

            $validatedData['illustration'] = $filename ;
        }

        $theGame = Game::create($validatedData);

        return redirect()->route('games.index')->with('success', 'Le jeu a bien été ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $game = Game::where('slug', $slug)->get()->first() ;

        if(is_null($game)) {
            return redirect()->route('games.index')->with('error', 'Le jeu en question n\'a pas été trouvé en base de données');
        }
        $owners = $game->owners;

        //TODO purger les champs confidentiels des users
        
        return view('game.details', [
            'game'          => $game,
            'owners'        => $owners
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }

        $game = Game::findOrFail($id) ;

        $characteristicsManager = new CharacteristicManager ;

        return view('game.edit', [
            'game'                      => $game,
            'gameTypesList'             => $characteristicsManager->getTypesList(),
            'gameAffrontementsList'     => $characteristicsManager->getAffrontementsList(),
            'gameFormatsList'           => $characteristicsManager->getFormatsList(),
            'gameCapacitesList'         => $characteristicsManager->getCapacitesList(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\GameRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GameRequest $request, $id) {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }

        $validatedData = $request->validated() ;
        
        if($request->hasFile('file')) {
            // Upload file
            $file = $request->file;
            $filename = Str::slug($validatedData['title'], '_') . '.' . $file->extension() ;
            $location = 'img/games_covers/';
            $file->move($location,$filename);

            $validatedData['illustration'] = $filename ;
        }

        $theGame = Game::findOrFail($id);
        //dd($validatedData);
        $theGame->update($validatedData);

        return redirect()->route('games.show', $theGame->slug)->with('success', 'Le jeu a bien été modifié');
    }

    /**
     * Soft delete the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (!Auth::check()) {
            dd("Vous êtes déconnecté·e, cette page n'est pas disponible");
        }

        $game = Game::findOrFail($id) ;
        $game->update([
            'deleted_at'    => now(),
            'is_available'  => 0
        ]);
        return redirect()->route('games.index')->with('success', 'Le jeu a bien été supprimé');
    }


    /* ---------------------------------------------------------------------------------- */
    /** 
    * @param  Illuminate\Http\Request $request
    */
    public function searchGames(Request $request) {
        return response()->json([
            'games' => $games
        ]);
    }

    
}
