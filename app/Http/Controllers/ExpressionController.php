<?php

namespace App\Http\Controllers;

use App\Models\Expression;
use Illuminate\Http\Request;
use App\Http\Requests\ExpressionRequest;

class ExpressionController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expression = Expression::findOrFail($id) ;

        return view('expression.edit', [
            'expression'                      => $expression,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\ExpressionRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExpressionRequest $request, $id)
    {
        $validatedData = $request->validated() ;
        
        if($request->hasFile('file')) {
            // Upload file
            $file = $request->file;
            $filename = "imgOfID_" . $id . '.' . $file->extension() ;
            if(array_key_exists("is_game_lynx", $validatedData) && $validatedData["is_game_lynx"] === 1) {
                $location = 'dematGame_lynx/';
            } else {
                $location = 'expressions/';
            }
            $file->move('img/'.$location,$filename);
            $validatedData['path_picture'] = $location.$filename ;
            unset($validatedData['file']);
        }

        $theExp = Expression::findOrFail($id);
        $theExp->update($validatedData);

        return redirect()->route('expressions.edit', $theExp->id)->with('success', 'L\'expression a bien été modifiée');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
