<?php

namespace App\Http\Middleware;

use Auth ;
use Closure;
use App\Models\User;
use Illuminate\Http\Request;

class Authenticate_admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (Auth::user()->role !== User::ROLE_ADMIN) {
            return redirect()->route('ludotheque');
        }

        return $next($request);
    }
}
