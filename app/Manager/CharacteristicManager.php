<?php

namespace App\Manager ;

use App\Models\Characteristic;

class CharacteristicManager {
    public function build() {
         
    }

    //choix unique
    public function getTypesList() {
        $list = Characteristic::where([
            ['is_available', "=", 1],
            ['deleted_at', '=', NULL],
            ['type', '=', 'type'],
        ])->get() ;

        return $list ;
    }

    //choix unique
    public function getFormatsList() {
        $list = Characteristic::where([
            ['is_available', "=", 1],
            ['deleted_at', '=', NULL],
            ['type', '=', 'format'],
        ])->get() ;

        return $list ;
    }

    //choix unique
    public function getAffrontementsList() {
        $list = Characteristic::where([
            ['is_available', "=", 1],
            ['deleted_at', '=', NULL],
            ['type', '=', 'affrontement'],
        ])->get() ;

        return $list ;
    }

    //choix multiples
    public function getCapacitesList() {
        $list = Characteristic::where([
            ['is_available', "=", 1],
            ['deleted_at', '=', NULL],
            ['type', '=', 'capacite'],
        ])->get() ;

        return $list ;
    }
}