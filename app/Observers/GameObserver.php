<?php

namespace App\Observers;

use App\Models\Game;

use Cocur\Slugify\Slugify ;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class GameObserver
{
    /**
     * Handle the Game "created" event.
     *
     * @param  \App\Models\Game  $game
     * @return void
     */
    public function created(Game $game)
    {
        //création du slug pour utilisation en url
        $slugifieur = new Slugify() ;
        $game->slug = $slugifieur->slugify($game->title);
        $game->save();

        //attribution à la personne qui crée l'entrée
        $user = Auth::user() ;
        $gameIds = [$game->id];
        $user->games()->attach($gameIds);
    }

    /**
     * Handle the Game "updated" event.
     *
     * @param  \App\Models\Game  $game
     * @return void
     */
    public function updated(Game $game)
    {
        $game->slug = Str::slug($game->title, '-') ;
        $game->saveQuietly() ; //pour éviter un effet de boucle où la sauvegarde redéclanche un observer "a été modifié"
    }

    /**
     * Handle the Game "deleted" event.
     *
     * @param  \App\Models\Game  $game
     * @return void
     */
    public function deleted(Game $game)
    {
        //
    }

    /**
     * Handle the Game "restored" event.
     *
     * @param  \App\Models\Game  $game
     * @return void
     */
    public function restored(Game $game)
    {
        //
    }

    /**
     * Handle the Game "force deleted" event.
     *
     * @param  \App\Models\Game  $game
     * @return void
     */
    public function forceDeleted(Game $game)
    {
        //
    }
}
