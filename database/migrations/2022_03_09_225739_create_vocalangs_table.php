<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVocalangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vocalangs', function (Blueprint $table) {
            $table->id();
            $table->string('label', 48) ;
            $table->string('iso_639_1', 4) ;
            $table->string('path_flag', 255) ;
            $table->boolean('is_available')->default(1) ;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vocalangs');
    }
}
