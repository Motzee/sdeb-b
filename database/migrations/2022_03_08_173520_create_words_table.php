<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWordsTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'words', function (Blueprint $table) {
            $table->id();
            //code ISO 639-1 pour la langue
            $table->string('label_fr', 255) ;
            $table->string('determinant_fr', 12)->nullable() ;
            $table->integer('verb_group')->nullable() ;
            $table->string('label_an', 255)->nullable() ;
            $table->string('determinant_an', 12)->default("the")->nullable() ;
            $table->string('label_de', 255)->nullable() ;
            $table->string('determinant_de', 12)->nullable() ;
            $table->string('label_br', 255)->nullable() ;
            $table->string('determinant_br', 12)->nullable() ;
            $table->string('label_eo', 255)->nullable() ;
            $table->string('determinant_eo', 12)->default("la")->nullable() ;
            $table->boolean('is_available')->default(1) ;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'words');
    }
}
