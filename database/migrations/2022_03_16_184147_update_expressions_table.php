<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateExpressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('expressions');

        Schema::create('expressions', function (Blueprint $table) {
            $table->id();

            //code ISO 639-1 pour la langue
            $table->string('label_fr', 255) ;
            $table->string('determinant_fr', 12)->nullable() ;
            $table->string('label_an', 255)->nullable() ;
            $table->string('determinant_an', 12)->default("the")->nullable() ;
            $table->string('label_de', 255)->nullable() ;
            $table->string('determinant_de', 12)->nullable() ;
            $table->string('label_br', 255)->nullable() ;
            $table->string('determinant_br', 12)->nullable() ;
            $table->string('label_eo', 255)->nullable() ;
            $table->string('determinant_eo', 12)->default("la")->nullable() ;

            $table->string('grammarytype')->nullable();
            $table->integer('pegi_vocable')->nullable();
            $table->integer('pegi_warn')->nullable();

            $table->string('path_picture')->nullable();

            $table->boolean('is_guessable')->default(1);
            $table->boolean('is_drawable')->default(1);
            $table->boolean('is_mimic')->default(1);
            $table->boolean('is_game_lynx')->default(0);
            $table->boolean('is_public')->default(1);
            $table->boolean('is_available')->default(1);

            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expressions');
    }
}
