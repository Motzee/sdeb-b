<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags_words', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_tag');
            $table->foreign('id_tag')->references('id')->on('tags');

            $table->unsignedBigInteger('id_word');
            $table->foreign('id_word')->references('id')->on('words');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags_words');
    }
}
