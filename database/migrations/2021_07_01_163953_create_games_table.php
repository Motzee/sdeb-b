<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('game_id_extensionOf')->nullable();
            $table->string('title')->unique();
            $table->string('slug')->nullable()->unique();
            $table->string('shortdescription')->nullable();
            $table->string('affrontementtype')->nullable();
            $table->string('capacityneeded')->nullable();
            $table->string('type')->nullable();
            $table->string('format')->nullable();
            $table->time('durationgame', $precision = 0)->nullable();
            $table->boolean('is_gamemasterneeded')->default(0);
            $table->integer('nbplayers_min');
            $table->integer('nbplayers_max');
            $table->integer('minimalage')->nullable();
            $table->text('content')->nullable();
            $table->text('rules_text')->nullable();
            $table->string('rules_video')->nullable();
            $table->string('illustration')->nullable();
            $table->boolean('is_available')->default(1);
            $table->timestamps();
            $table->softDeletes();

            /* foreign keys */
            $table->foreign('game_id_extensionOf')->references('id')->on('games');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
