<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('words', function (Blueprint $table) {
            //ajout d'une foreign vers le type de mot
            $table->unsignedBigInteger('id_typeOfWord')->nullable();
            $table->foreign('id_typeOfWord')->references('id')->on('type_of_words');

            //ajout d'une foreign vers l'image
            $table->unsignedBigInteger('id_picture')->nullable();
            $table->foreign('id_picture')->references('id')->on('pictures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('words', function (Blueprint $table) {
            $table->dropForeign(['id_typeOfWord']);
            $table->dropForeign(['id_picture']);
        });
        
    }
}
