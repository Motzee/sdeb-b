<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expressions', function (Blueprint $table) {
            $table->id();
            $table->string('label')->unique();
            $table->boolean('is_guessable')->default(1);
            $table->boolean('is_drawable')->default(1);
            $table->boolean('is_mimic')->default(1);
            $table->boolean('is_game_lynx')->default(0);
            $table->string('type_grammary')->nullable();
            $table->string('picture')->nullable();
            $table->integer('pegi_vocable')->nullable();
            $table->integer('pegi_warn')->nullable();
            $table->boolean('is_available')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

/* type grammaire
- nom
- adjectif
- verbe
*/


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expressions');
    }
}
