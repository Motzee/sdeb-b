<?php

namespace Database\Seeders;

use App\Models\Vocalang;
use Illuminate\Database\Seeder;

class VocalangsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vocalang::create([
            'id'                => 1,
            'label'             => 'français',
            'iso_639_1'         => 'fr',
            'path_flag'         => '/img/flags/drapeau_fr.png',
            'is_available'      => 1
        ]);
        Vocalang::create([
            'id'                => 2,
            'label'             => 'anglais',
            'iso_639_1'         => 'an',
            'path_flag'         => '/img/flags/drapeau_an.png',
            'is_available'      => 1
        ]);
        Vocalang::create([
            'id'                => 3,
            'label'             => 'allemand',
            'iso_639_1'         => 'de',
            'path_flag'         => '/img/flags/drapeau_de.png',
            'is_available'      => 1
        ]);
        Vocalang::create([
            'id'                => 4,
            'label'             => 'espéranto',
            'iso_639_1'         => 'eo',
            'path_flag'         => '/img/flags/drapeau_eo.png',
            'is_available'      => 0
        ]);
        Vocalang::create([
            'id'                => 5,
            'label'             => 'breton',
            'iso_639_1'         => 'br',
            'path_flag'         => '/img/flags/drapeau_br.png',
            'is_available'      => 0
        ]);
    
        
    }
}
