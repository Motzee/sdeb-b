<?php

namespace Database\Seeders;

use App\Models\User ;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        User::truncate();
        Schema::enableForeignKeyConstraints();

        User::create([
            'id'                => 1,
            'name'              => 'Maelle',
            'email'             => 'maelle.perron.26@gmail.com',
            'password'          => Hash::make('Bépo1234'),
            'role'              => User::ROLE_ADMIN,
            'avatar'            => 'maelle.jpg'
        ]);

        User::create([
            'id'             => 2,
            'name'           => 'Christian',
            'email'          => 'test@test.com',
            'password'       => Hash::make('Azerty1234'),
            'role'           => User::ROLE_USER,
        ]);
    }
}
