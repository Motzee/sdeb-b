<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GameUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('game_user')->delete();
        
        \DB::table('game_user')->insert(array (
            0 => 
            array (
                'id' => 1,
                'game_id' => 1,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'game_id' => 2,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'game_id' => 4,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'game_id' => 5,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'game_id' => 6,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'game_id' => 3,
                'user_id' => 2,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'game_id' => 4,
                'user_id' => 2,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'game_id' => 7,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'game_id' => 8,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'game_id' => 9,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'game_id' => 10,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'game_id' => 11,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'game_id' => 12,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'game_id' => 13,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'game_id' => 14,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'game_id' => 15,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'game_id' => 16,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'game_id' => 17,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'game_id' => 18,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'game_id' => 19,
                'user_id' => 1,
                'user_id_borrower' => NULL,
                'externalborrower_name' => NULL,
                'content' => NULL,
                'is_available' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}