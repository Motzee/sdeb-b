<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Database\Migrations\CreateTypeOfWordsTable ;

class TypeOfWordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // /!\ DO NOT CHANGE the order in this table
        $types = [
            "nom",
            "pronom",
            "déterminant",
            "adjectif",
            "verbe",
            "adverbe",
            "préposition",
            "conjonction",
            "interjection"
        ] ;
        
        for($i = 1 ; $i <= count($types) ; $i++) {
            DB::table('type_of_words')->insert([
                'id' => $i,
                'type_FR' => $types[$i - 1]
            ]);
        }
        
    }
}
