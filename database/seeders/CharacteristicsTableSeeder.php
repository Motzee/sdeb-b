<?php

namespace Database\Seeders;

use App\Models\Characteristic;
use Illuminate\Database\Seeder;

class CharacteristicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('characteristics')->delete();
        
        /*  ------------FORMAT------------ */
        Characteristic::create([
            'id'                => 1,
            'label'             => 'Jeu de plateau',
            'type'              => 'format',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 2,
            'label'             => 'Jeux de dés',
            'type'              => 'format',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 3,
            'label'             => 'Jeux de cartes',
            'type'              => 'format',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 4,
            'label'             => 'Jeu de pions/figurines',
            'type'              => 'format',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 5,
            'label'             => 'Jeu de matériel à manipuler',
            'type'              => 'format',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 6,
            'label'             => 'Jeu d\'éléments assemblables',
            'type'              => 'format',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 7,
            'label'             => 'Jeu d\'ardoises',
            'type'              => 'format',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 8,
            'label'             => 'Jeu dématérialisé',
            'type'              => 'format',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 9,
            'label'             => 'Jeu sans matériel',
            'type'              => 'format',
            'is_available'      => 1,
        ]);


        /*  ------------TYPE D'AFFRONTEMENT------------ */
        Characteristic::create([
            'id'                => 10,
            'label'             => 'Affrontement individuel',
            'type'              => 'affrontement',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 11,
            'label'             => 'Affrontement par équipes',
            'type'              => 'affrontement',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 12,
            'label'             => 'Ennemi commun',
            'type'              => 'affrontement',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 13,
            'label'             => 'Coopération',
            'type'              => 'affrontement',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 14,
            'label'             => 'Victoire ou défaite collective',
            'type'              => 'affrontement',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 15,
            'label'             => 'Jeu individualisé',
            'type'              => 'affrontement',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 16,
            'label'             => 'Sans défaite',
            'type'              => 'affrontement',
            'is_available'      => 1,
        ]);


        /*  ------------TYPE------------ */
        Characteristic::create([
            'id'                => 17,
            'label'             => 'Construction de deck',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 18,
            'label'             => 'Enquête',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 19,
            'label'             => 'Bluff',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 20,
            'label'             => 'Jeu de rôles',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 21,
            'label'             => 'Jeu d\'ambiance',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 22,
            'label'             => 'Jeu de devinettes/faire deviner',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 23,
            'label'             => 'Support éducatif',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 24,
            'label'             => 'Jeu sportif/d\'extérieur',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 25,
            'label'             => 'Jeu de chiffres/lettres',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 26,
            'label'             => 'Jeu libre',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 27,
            'label'             => 'Jeu de construction',
            'type'              => 'type',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 28,
            'label'             => 'Jeu à règle',
            'type'              => 'type',
            'is_available'      => 1,
        ]);


        /*  ------------CAPACITÉ------------ */
        Characteristic::create([
            'id'                => 29,
            'label'             => 'Chance/hasard',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 30,
            'label'             => 'Stratégie',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 31,
            'label'             => 'Gestion',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 32,
            'label'             => 'Adresse',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 33,
            'label'             => 'Motricité fine',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 34,
            'label'             => 'Équilibre',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 35,
            'label'             => 'Connaissance',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 36,
            'label'             => 'Observation',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 37,
            'label'             => 'Déduction, logique',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 38,
            'label'             => 'Mémoire',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 39,
            'label'             => 'Rapidité',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 40,
            'label'             => 'Communication',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 41,
            'label'             => 'Créativité',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
        Characteristic::create([
            'id'                => 42,
            'label'             => 'Repérage visuo-spatial',
            'type'              => 'capacite',
            'is_available'      => 1,
        ]);
    }
}
