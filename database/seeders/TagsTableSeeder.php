<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // /!\ DO NOT CHANGE the order in this table
        $tags = [
            "les personnes",
            "la famille",
            "les vêtements",
            "les couleurs",
            "la maison",
            "la cuisine",
            "le restaurant",
            "les aliments",
            "les fruits",
            "les légumes",
            "les boissons",
            "les aliments divers",
            "les animaux",
            "les animaux d'élevage",
            "les animaux domestiques",
            "les animaux sauvages",
            "le corps",
            "le voyage",
            "les transports",
            "la mer",
            "la montagne",
            "la nature",
            "la ville",
            "les pays",
            "les saisons et la météo",
            "les fêtes",
            "Noël",
            "Halloween",
            "les métiers",
            "l'école",
            "l'entreprise",
            "les objets",
            ""
        ] ;
        
        for($i = 1 ; $i <= count($tags) ; $i++) {
            DB::table('tags')->insert([
                'id' => $i,
                'label' => $tags[$i - 1]
            ]);
        }
    }
}
