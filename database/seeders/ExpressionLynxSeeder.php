<?php

namespace Database\Seeders;

use App\Models\Expression;
use Illuminate\Database\Seeder;

class ExpressionLynxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //NB : on ne vide pas la table préalablement car cette classe est appelée par un autre seeder où c'est déjà fait

        Expression::create([
            'label_fr'             => 'origami (papier plié)',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 0,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-1.png',
            'pegi_vocable'      => 7,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'mathématiques',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 0,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-2.png',
            'pegi_vocable'      => 7,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'brocoli',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 0,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-3.png',
            'pegi_vocable'      => 5,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'travailleur de chantier',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 1,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-4.png',
            'pegi_vocable'      => 5,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'nuage de pluie',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 0,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-5.png',
            'pegi_vocable'      => 3,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'hérisson',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 1,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-6.png',
            'pegi_vocable'      => 3,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'bobines de fil',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 0,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-7.png',
            'pegi_vocable'      => 5,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'potiron',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 0,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-8.png',
            'pegi_vocable'      => 5,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'gland',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 0,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-9.png',
            'pegi_vocable'      => 7,
            'pegi_warn'         => 0
        ]);
        Expression::create([
            'label_fr'             => 'banana split (dessert)',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 0,
            'is_game_lynx'      => 1,
            'grammarytype'     => 'nom substantif',
            'path_picture'           => 'dematGame_lynx/img-10.png',
            'pegi_vocable'      => 9,
            'pegi_warn'         => 0
        ]);

        for($i = 11; $i <= 700 ; $i++) {
            Expression::create([
                'label_fr'             => 'image '.$i,
                'is_guessable'      => 1,
                'is_drawable'       => 1,
                'is_mimic'          => 0,
                'is_game_lynx'      => 1,
                'grammarytype'     => 'nom substantif',
                'path_picture'           => 'dematGame_lynx/img-'.$i.'.png',
                'pegi_vocable'      => 99,
                'pegi_warn'         => 99
            ]);
        }
    }
}
