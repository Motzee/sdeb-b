<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Expression ;

class ExpressionSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Expression::truncate();

        //lot spécial jeu du lynx
        $this->call(ExpressionLynxSeeder::class);

        Expression::create([
            'label_fr'          => 'danser',
            'is_guessable'      => 1,
            'is_drawable'       => 1,
            'is_mimic'          => 1,
            'is_game_lynx'      => 0,
            'grammarytype'     => 'verbe',
            'pegi_vocable'      => 3,
            'pegi_warn'         => 0
        ]);

        
    }
}
