<?php

use App\Models\Game;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\GameCollection;
use App\Manager\CharacteristicManager;
use App\Http\Controllers\GameController ;
use App\Http\Controllers\ExpressionController;
use App\Http\Controllers\WordController;
use App\Http\Controllers\PictureController;
use App\Http\Controllers\ParametersController ;
use App\Http\Controllers\DematerializedGamesController ;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('accueil', [
        
    ]);
})->name('accueil');

Route::get('/dashboard', function () {
    //return Redirect::route('');
    return redirect()->route('games.index');
})->name('dashboard');

/* ---------------------- Membres connectés ---------------------- */
Route::get('/profile', [ParametersController::class, 'userProfile'])->middleware(['auth'])->name('userProfile');
Route::put('/profile', [ParametersController::class, 'updateProfile'])->middleware(['auth'])->name('updateUserProfile');

//jeux dématérialisés
Route::get('/dematerialized-games/lynx', [DematerializedGamesController::class, 'lynx'])->name('dematerialized-games.lynx');

Route::get('/dematerialized-games/vocabulangue', [DematerializedGamesController::class, 'vocalang'])->name('dematerialized-games.vocalang');
Route::get('/dematerialized-games/vocabulangue/{language_iso}/{chosenLanguage_iso}', [DematerializedGamesController::class, 'chosenVocalang'])->where('language_iso', '[A-Za-z]+')->where('chosenLanguage_iso', '[A-Za-z]+')->name('dematerialized-games.chosenVocalang');

Route::resources([
    'games'         => GameController::class,
    'expressions'   => ExpressionController::class,
    'words'         => WordController::class,
    'pictures'      => PictureController::class,
]);

/* ---------------------- Zone ADMIN ---------------------- */


Route::prefix('admin')->middleware(['auth_admin'])->group(function () {
    Route::get('/administration', [ParametersController::class, 'administration'])->name('admin.administration');
    Route::get('/expressions', [ParametersController::class, 'administrateExpressions'])->name('admin.expressions');
});



require __DIR__.'/auth.php';